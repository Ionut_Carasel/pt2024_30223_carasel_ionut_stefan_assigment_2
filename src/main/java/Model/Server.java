package Model;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;


public class Server implements Runnable{
    private ConcurrentLinkedQueue<Task> tasks = new ConcurrentLinkedQueue<>();
    private AtomicInteger waitingTime;

    public Server(){
        this.waitingTime = new AtomicInteger(0);
        this.tasks = new ConcurrentLinkedQueue<>();
    }

    public ConcurrentLinkedQueue<Task> getTasks() {
        return tasks;
    }

    public int getIntWaitingTime() {
        return waitingTime.get();
    }

    public AtomicInteger getWaitingTime() {
        return waitingTime;
    }


    public void addTask(Task task) {
        tasks.add(task);
        this.waitingTime.addAndGet(task.getServiceTime());
    }

    @Override
    public void run() {
        System.out.println("Server is processing");
        while(!this.tasks.isEmpty()){
            Task newTask = tasks.peek();
            System.out.println("Thread "+ Thread.currentThread().getId() +" Wait time " +newTask.getServiceTime());
            try {
                Thread.sleep(newTask.getServiceTime()*1000L);
                this.waitingTime.addAndGet(-newTask.getServiceTime());

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            tasks.poll();
        }
    }
}
