package Model;

public class Parameters {
    private final int simulationInterval;
    private final int minArrivalTime;
    private final int maxArrivalTime;
    private final int minServiceTime;
    private final int maxServiceTime;

    public Parameters(int simulationInterval, int minArrivalTime,int maxArrivalTime,int minServiceTime,int maxServiceTime) {
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.simulationInterval = simulationInterval;
    }

    public int getSimulationInterval() {
        return simulationInterval;
    }


    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public int getMinServiceTime() {
        return minServiceTime;
    }

    public int getMaxServiceTime() {
        return maxServiceTime;
    }
}
