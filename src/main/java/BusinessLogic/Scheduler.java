package BusinessLogic;
import Model.Server;
import Model.Task;

import java.util.*;
import java.util.concurrent.*;


public class Scheduler {
    private ArrayList<Server> servers = new ArrayList<>();
    private final int maxNumberOfServers;
    private Strategy strategy;
    private SelectionPolicy strategyStatus;

    ThreadPoolExecutor service;

    public Scheduler(int maxNumberOfServers) {
        this.maxNumberOfServers = maxNumberOfServers;
        this.servers = new ArrayList<Server>(maxNumberOfServers);
        for (int i = 0; i < maxNumberOfServers; i++) {
            servers.add(new Server());
        }
        this.service = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxNumberOfServers);
        this.strategy = new TimeStrategy();
        this.strategyStatus = SelectionPolicy.SHORTEST_TIME;
    }

    public void executeThreadPool(){
        for(Server server : servers){
            service.execute(server);
        }
    }

    public void shutDownThreadPool(){
        this.service.shutdown();
        while (true) {
            try {
                if (this.service.awaitTermination(1, TimeUnit.SECONDS)) break;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Awaiting termination");
        }
    }

    public ArrayList<Server> getServers() {
        return servers;
    }

    public void changeStrategy(SelectionPolicy policy) {
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ShQueueStrategy();
            strategyStatus = SelectionPolicy.SHORTEST_QUEUE;
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new TimeStrategy();
            strategyStatus = SelectionPolicy.SHORTEST_TIME;
        }
    }

    public boolean toChangeOrNotToChange(){
        int minTime = Integer.MAX_VALUE;
        int counter = 0;
        for(Server server : servers){
            if(server.getIntWaitingTime()<minTime){
                minTime = server.getIntWaitingTime();
                counter = 1;
            }
            else if(server.getIntWaitingTime()==minTime){
                counter++;
            }
        }
        return counter > 1;
    }

    public void dispachTask(Task task){
        if(this.toChangeOrNotToChange()){
            this.changeStrategy(SelectionPolicy.SHORTEST_QUEUE);
        }
        System.out.println("Scheduler to the rescue");
        this.strategy.addNewTask(this.servers,task);
        if(this.strategyStatus == SelectionPolicy.SHORTEST_QUEUE){
            this.changeStrategy(SelectionPolicy.SHORTEST_TIME);
        }
    }
}
