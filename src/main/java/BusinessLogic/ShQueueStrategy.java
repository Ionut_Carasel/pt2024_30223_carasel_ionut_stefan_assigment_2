package BusinessLogic;

import Model.Server;
import Model.Task;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ShQueueStrategy implements Strategy{
    @Override
    public void addNewTask(ArrayList<Server> servers, Task newTask) {
        if(!servers.isEmpty()){
            int minLengthIndex = 0;
            int minLength = Integer.MAX_VALUE;
            for (Server server : servers) {
                if(server.getTasks().size()<minLength){
                    minLength = server.getTasks().size();
                    minLengthIndex = servers.indexOf(server);
                }
            }
            servers.get(minLengthIndex).addTask(newTask);
        }
    }
}
