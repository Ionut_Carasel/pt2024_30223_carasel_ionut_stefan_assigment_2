package BusinessLogic;

import Model.Parameters;
import Model.Server;
import Model.Task;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class SimulationManager implements Runnable {

    public Parameters parameters;
    public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
    private Scheduler scheduler;
    private ArrayList<Task> generatedTasks;

    public SimulationManager(int numberOfServers, int numberOfTasks, Parameters param) {
        this.parameters = param;
        this.scheduler = new Scheduler(numberOfServers);
        this.generatedTasks = generateTasks(numberOfTasks);
    }

    private ArrayList<Task> generateTasks(int numberOfTasks) {
        ArrayList<Task> tasks = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < numberOfTasks; i++) {
            int arrival = rand.nextInt(parameters.getMinArrivalTime(), parameters.getMaxArrivalTime());
            int service = rand.nextInt(parameters.getMinServiceTime(), parameters.getMaxServiceTime());
            Task newTask = new Task(i, arrival, service);
            tasks.add(newTask);
        }
        Comparator<Task> comparator = Comparator.comparingInt(Task::getArrivalTime);
        tasks.sort(comparator);
        return tasks;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    @Override
    public void run() {
        int currentTime = 0;

        while (currentTime <= parameters.getSimulationInterval()) {
            System.out.println(currentTime);
            this.scheduler.executeThreadPool();
            while (!this.generatedTasks.isEmpty() && this.generatedTasks.getFirst().getArrivalTime() == currentTime) {
                this.scheduler.dispachTask(this.generatedTasks.getFirst());
                this.generatedTasks.remove(this.generatedTasks.getFirst());
            }

            LogWriter.writeLog(currentTime, this.generatedTasks, this.scheduler.getServers());
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            currentTime++;
        }
        this.scheduler.shutDownThreadPool();
    }
}
