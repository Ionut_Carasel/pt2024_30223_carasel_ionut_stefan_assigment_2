package GUI;

import BusinessLogic.SimulationManager;
import Model.Parameters;

import javax.swing.*;
import java.awt.*;

public class QueueInterface extends JDialog{

    private JTextField textFieldServNo;
    private JTextField textFieldMinArrival;
    private JTextField textFieldMaxArrival;
    private JTextField textFieldMinTask;
    private JTextField textFieldMaxtask;
    private JButton simuleazaButton;
    private JPanel simulationField;
    private JTextField textFieldNrClienti;
    private JTextField textFieldSim;
    private JPanel contentPanel;
    private JLabel Mesaj;

    public QueueInterface(JDialog parent){
        super(parent);
        setTitle("Queue Simulator");
        setContentPane(contentPanel);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        simuleazaButton.addActionListener(e->{
            boolean prodeedBool = true;
            if(textFieldServNo.getText().isBlank()){
                prodeedBool = false;
            }
            else if(!textFieldServNo.getText().matches("(\\d+)")){
                prodeedBool = false;
            }
            if(textFieldMinArrival.getText().isBlank()){
                prodeedBool = false;
            }
            else if(!textFieldMinArrival.getText().matches("(\\d+)")){
                prodeedBool = false;
            }
            if(textFieldMaxArrival.getText().isBlank()){
                prodeedBool = false;
            }
            else if(!textFieldMaxArrival.getText().matches("(\\d+)")){
                prodeedBool = false;
            }
            if(textFieldMinTask.getText().isBlank()){
                prodeedBool = false;
            }
            else if(!textFieldMinTask.getText().matches("(\\d+)")){
                prodeedBool = false;
            }
            if(textFieldMaxtask.getText().isBlank()){
                prodeedBool = false;
            }
            else if(!textFieldMaxtask.getText().matches("(\\d+)")){
                prodeedBool = false;
            }
            if(prodeedBool){
                Integer clientNo=Integer.parseInt(textFieldNrClienti.getText());
                Integer servNo=Integer.parseInt(textFieldServNo.getText());
                Integer minArrival=Integer.parseInt(textFieldMinArrival.getText());
                Integer maxArrival=Integer.parseInt(textFieldMaxArrival.getText());
                Integer minTask=Integer.parseInt(textFieldMinTask.getText());
                Integer maxTask=Integer.parseInt(textFieldMaxtask.getText());
                Integer simulationTime=Integer.parseInt(textFieldSim.getText());
                Parameters simulationParameters=new Parameters(simulationTime,minArrival,maxArrival,minTask,maxTask);
                dispose();
                SimulationManager simulationManager=new SimulationManager(servNo,clientNo,simulationParameters);
                simulationManager.run();
            }

            else{
                Mesaj.setText("Date invalide");
            }
        });

        setVisible(true);
    }
}
