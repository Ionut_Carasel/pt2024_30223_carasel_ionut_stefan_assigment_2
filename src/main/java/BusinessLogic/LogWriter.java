package BusinessLogic;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import Model.Server;
import Model.Task;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LogWriter {
    public static void writeLog(int currentTime, ArrayList<Task> taskList, ArrayList<Server> serverList) {
        BufferedWriter writer = null;
        StringBuilder outputBuffer = new StringBuilder();
        outputBuffer.append("Time: " + currentTime + "\n");
        outputBuffer.append("Waiting Clients: ");
        for (Task task : taskList) {
            outputBuffer.append("(" + task.getID() + "," + task.getArrivalTime() + "," + task.getServiceTime() + ")");
        }
        outputBuffer.append("\n");
        for (int i = 0; i < serverList.size(); i++) {
            outputBuffer.append("Queue " + (i+1) + ":");
            for (Task t : serverList.get(i).getTasks()) {
                outputBuffer.append("(" + t.getID() + "," + t.getArrivalTime() + "," + t.getServiceTime() + ")");
            }
            outputBuffer.append("\n");
        }
        try {
            writer = new BufferedWriter(new FileWriter("log.txt", true));
            writer.write(outputBuffer.toString());
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
