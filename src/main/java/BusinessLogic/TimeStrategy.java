package BusinessLogic;

import Model.Server;
import Model.Task;

import java.util.ArrayList;
import java.util.Collections;

public class TimeStrategy implements Strategy{
    @Override
    public void addNewTask(ArrayList<Server> servers, Task newTask) {
        if(!servers.isEmpty()){
            System.out.println("Dispatching client");
            int minTimeIndex = 0;
            int minTime = Integer.MAX_VALUE;
            for (Server server : servers) {
                if(server.getIntWaitingTime()<minTime){
                    minTime = server.getIntWaitingTime();
                    minTimeIndex = servers.indexOf(server);
                }
            }
            servers.get(minTimeIndex).addTask(newTask);
        } else {
            System.out.println("No servers available");
        }
    }
}
