package Model;

public class Task {
    private final int clientID;
    private final int arrivalTime;
    private final int serviceTime;


    public Task(int ID, int arrivalTime, int serviceTime) {
        this.clientID = ID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }


    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getID() {
        return clientID;
    }

    public int getServiceTime() {
        return serviceTime;
    }

}
