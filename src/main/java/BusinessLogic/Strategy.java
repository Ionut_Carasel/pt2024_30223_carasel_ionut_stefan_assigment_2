package BusinessLogic;
import Model.Server;
import Model.Task;

import java.util.*;
public interface Strategy {
    public void addNewTask(ArrayList<Server> servers, Task newTask);
}
